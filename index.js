var numberArr = [];

function themSo() {
    var number = document.querySelector("#txt-number").value * 1;

    numberArr.push(number)
    document.querySelector("#txt-number").value = ""
    document.querySelector("#result").innerHTML = `${numberArr}`
}
// bài 1
function tinhTong() {
    var tongSoDuong = 0;
    numberArr.forEach(function (number) {
        if (number > 0) {
            tongSoDuong += number
        }
    })
    document.querySelector("#result1").innerHTML = `<p>Tổng số dương: ${tongSoDuong}</p>`
}
// bài 2
function demSo() {
    var soLuongSoDuong = 0
    numberArr.forEach(function (number) {
        if (number > 0) {
            soLuongSoDuong++
        }
    })
    // const soLuongSoDuong = numberArr.filter(function(number) {
    //     return number > 0
    // }).length;
    document.querySelector("#result2").innerHTML = `<p>Số dương: ${soLuongSoDuong}</p>`
}
//bài 3
function timSoNhoNhat() {
    var soNhoNhat = numberArr[0];

    numberArr.forEach(function (number) {
        if (number < soNhoNhat) {
            soNhoNhat = number
        }
    })
    document.querySelector("#result3").innerHTML = `<p>Số nhỏ nhất: ${soNhoNhat}</p>`
}
//bài 4
function timSoDuongNhoNhat() {
    // var soDuong = [];
    // for (index = 0; index < numberArr.length; index++) {
    //     if (numberArr[index] > 0) {
    //         soDuong.push(numberArr[index])
    //     }
    //     console.log('soDuong: ', soDuong);
    // }
    // var soDuongNhoNhat = soDuong[0]
    // for (i = 0; i < soDuong.length; i++) {
    //     var soDuongHienTai = soDuong[i]
    //     if (soDuongHienTai < soDuongNhoNhat) {
    //         soDuongNhoNhat = soDuongHienTai
    //     }
    // }
    const arrDuong = numberArr.filter(function (number) {
        var soDuong = number > 0;
        return soDuong;
    })
    var soDuongNhoNhat = arrDuong[0]
    arrDuong.forEach(function (number) {
        if (number < soDuongNhoNhat) {
            soDuongNhoNhat = number;
        }
    })
    document.querySelector("#result4").innerHTML = `Số dương nhỏ nhất ${soDuongNhoNhat}`
}
//bài 5
function timSoChan() {
    var result = 0;
    const arrChan = numberArr.filter(function (number) {
        var soChan = number % 2 == 0
        return soChan
    })
    if (arrChan.length == 0) {
        result = -1;
    } else {
        result = arrChan[arrChan.length - 1]
    }
    document.querySelector("#result5").innerHTML = `Số chẵn cuối cùng: ${result}`
}
//bài 6
function doiCho() {
    var viTri1 = document.getElementById("viTri1").value * 1
    var viTri2 = document.getElementById("viTri2").value * 1
    var temp = numberArr[viTri1];
    numberArr[viTri1] = numberArr[viTri2];
    numberArr[viTri2] = temp;
    document.querySelector("#result6").innerHTML = `Mảng sau khi đổi: ${numberArr}`
}
//bài 7
function sapXep() {
    for (var i = 0; i < numberArr.length; i++) {
        var current = numberArr[i];
        var next = numberArr[i + 1];
        var temp = current;
        if (current > next) {
            numberArr[i] = numberArr[i + 1];
            numberArr[i + 1] = temp;
        }
    }
    console.log('numberArr: ', numberArr);
}

function kiemTraSoNgTo(n) {
    var flag = true;

    if (n < 2) {
        flag = false;
    } else {
        for (var i = 2; i < n - 1; i++) {
            if (n % i == 0) {
                flag = false;
                break;
            }
        }
    }

    return flag;
}

function soNguyenTo() {
    const soNgToArr = [];
    var result;
    for (var i = 0; i < numberArr.length; i++) {
        if (kiemTraSoNgTo(numberArr[i])) {
            soNgToArr.push(numberArr[i]);
        }
    }
    if (soNgToArr.length == 0) {
        result = -1;
    } else {
        result = soNgToArr[0]
    }
    document.getElementById("result8").innerHTML = `Số nguyên tố: ${result}`
}
//bài 9
var numberArr_2 = []
function them() {
    var so = document.getElementById("txt-number-1").value * 1;
    numberArr_2.push(so);
    document.getElementById("result9").innerHTML = `${numberArr_2}`;
}
function soNguyen() {
    var soNguyen = numberArr_2.filter(function (number) {
        var dieuKienSoNguyen = Number.isInteger(number)
        return dieuKienSoNguyen
    });
    var demSoNguyen = 0;
    demSoNguyen = soNguyen.length;
    document.getElementById("result9_1").innerHTML = `Số lượng số nguyên: ${demSoNguyen}`
}
//bài 10
function soSanh() {
    var soDuong = numberArr.filter(function (number) {
        var dieuKienSoDuong = number > 0
        return dieuKienSoDuong
    })
    var soAm = numberArr.filter(function (number) {
        var dieuKienAm = number < 0
        return dieuKienAm
    })
    var demSoDuong = soDuong.length;
    var demSoAm = soAm.length;
    var symbol = "";
    if (demSoDuong > demSoAm) {
        symbol = ">"
    } else if (demSoDuong == demSoAm) {
        symbol = "="
    } else {
        symbol = "<"
    }
    document.getElementById("result10").innerHTML = `số dương ${symbol} số âm`
}
